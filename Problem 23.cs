﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem23
{
    public class Program
    {
        // A perfect number is a number for which the sum of its proper divisors is exactly equal to the number. 
        // For example, the sum of the proper divisors of 28 would be 1 + 2 + 4 + 7 + 14 = 28, which means that 28 is a perfect number.

        // A number n is called deficient if the sum of its proper divisors is less than n and it is called abundant if this sum exceeds n.

        // As 12 is the smallest abundant number, 1 + 2 + 3 + 4 + 6 = 16, the smallest number that can be written as the sum of two abundant numbers is 24.
        // By mathematical analysis, it can be shown that all integers greater than 28123 can be written as the sum of two abundant numbers.
        // However, this upper limit cannot be reduced any further by analysis even though it is known that the greatest number that cannot be expressed
        // as the sum of two abundant numbers is less than this limit.

        // Find the sum of all the positive integers which cannot be written as the sum of two abundant numbers.

        public static void Main(string[] args)
        {
            int upperLimit = 28123;
            int sumOfNums = 0;
            List<int> abundantNumbers = new List<int>();
            bool[] sums = new bool[upperLimit + 1];

            for (int i = 0; i <= upperLimit; i++)
            {
                int sum = 0;
                for (int j = 2; j <= (i/2); j++) {
                    if (i % j == 0)
                        sum += j;
                }
                if (sum > i)
                    abundantNumbers.Add(i);
            }

            for (int i = 0; i < abundantNumbers.Count; i++) {
                for (int j = i; j < abundantNumbers.Count; j++) {
                    if ((abundantNumbers[i] + abundantNumbers[j]) < upperLimit)
                        sums[abundantNumbers[i] + abundantNumbers[j]] = true;
                    else
                        break;
                }
            }

            for (int i = 0; i < upperLimit; i++)
            {
                if (sums[i] == false)                
                    sumOfNums += i;                
            }

            Console.WriteLine("Sum of Numbers: {0}", sumOfNums);
            Console.ReadLine();
        }
    }
}
 