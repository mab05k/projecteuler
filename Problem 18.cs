﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem18
{
    public class Program
    {

//        By starting at the top of the triangle below and moving to adjacent numbers on the row below, the maximum total from top to bottom is 23.

//3
//7 4
// 2 4 6
// 8 5 9 3

//That is, 3 + 7 + 4 + 9 = 23.

//Find the maximum total from top to bottom of the triangle below:

//75
// 95 64
// 17 47 82
// 18 35 87 10
// 20 04 82 47 65
// 19 01 23 75 03 34
// 88 02 77 73 07 63 67
// 99 65 04 28 06 16 70 92
// 41 41 26 56 83 40 80 70 33
// 41 48 72 33 47 32 37 16 94 29
// 53 71 44 65 25 43 91 52 97 51 14
// 70 11 33 28 77 73 17 78 39 68 17 57
// 91 71 52 38 17 14 91 43 58 50 27 29 48
// 63 66 04 68 89 53 67 30 73 16 69 87 40 31
// 04 62 98 27 23 09 70 98 73 93 38 53 60 04 23

//NOTE: As there are only 16384 routes, it is possible to solve this problem by trying every route. However, Problem 67, is the same challenge with a triangle containing one-hundred rows; it cannot be solved by brute force, and requires a clever method! ;o)

        public static void Main(string[] args)
        {
            int[][] triangle = new int[15][];

            triangle[0] = new int[] { 75 };
            triangle[1] = new int[] { 95, 64 };
            triangle[2] = new int[] { 17, 47, 82 };
            triangle[3] = new int[] { 18, 35, 87, 10 };
            triangle[4] = new int[] { 20, 04, 82, 47, 65 };
            triangle[5] = new int[] { 19, 01, 23, 75, 03, 34 };
            triangle[6] = new int[] { 88, 02, 77, 73, 07, 63, 67 };
            triangle[7] = new int[] { 99, 65, 04, 28, 06, 16, 70, 92 };
            triangle[8] = new int[] { 41, 41, 26, 56, 83, 40, 80, 70, 33 };
            triangle[9] = new int[] { 41, 48, 72, 33, 47, 32, 37, 16, 94, 29 };
            triangle[10] = new int[] { 53, 71, 44, 65, 25, 43, 91, 52, 97, 51, 14 };
            triangle[11] = new int[] { 70, 11, 33, 28, 77, 73, 17, 78, 39, 68, 17, 57 };
            triangle[12] = new int[] { 91, 71, 52, 38, 17, 14, 91, 43, 58, 50, 27, 29, 48 };
            triangle[13] = new int[] { 63, 66, 04, 68, 89, 53, 67, 30, 73, 16, 69, 87, 40, 31 };
            triangle[14] = new int[] { 04, 62, 98, 27, 23, 09, 70, 98, 73, 93, 38, 53, 60, 04, 23 };

            Stopwatch sw = new Stopwatch();
            sw.Start();
            for (int i = triangle.Length - 2; i >= 0; i--) {
                for (int j = 0; j < triangle[i].Length; j++) {
                    triangle[i][j] += Math.Max(triangle[i + 1][j], triangle[i + 1][j + 1]);
                }
            }
            sw.Stop();
            
            Console.WriteLine("Max value: {0}", triangle[0][0]);
            Console.WriteLine("Elapsed: {0} ticks", sw.ElapsedTicks);
            Console.ReadLine();
        }
    }
}
