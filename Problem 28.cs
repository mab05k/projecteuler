﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem28
{
    class Program
    {
        // Starting with the number 1 and moving to the right in a clockwise direction a 5 by 5 spiral is formed as follows:

        // 21 22 23 24 25
        // 20  7  8  9 10
        // 19  6  1  2 11
        // 18  5  4  3 12
        // 17 16 15 14 13

        // It can be verified that the sum of the numbers on the diagonals is 101.

        // What is the sum of the numbers on the diagonals in a 1001 by 1001 spiral formed in the same way?

        static void Main(string[] args)
        {
            double sum = 1;
            int num = 1;

            int sides = 1001;
            int center = sides /2;
            int[,] grid = new int[sides,sides];
            int lastX = center;
            int lastY = center;

            int Right = 1;
            int Down = 1;
            int Left = 2;
            int Up = 2;

            Stopwatch sw = new Stopwatch();
            sw.Start();
            
            grid[center, center] = num; // Center
            num++;
            grid[lastX, lastY + 1] = num; // 2
            lastY += 1;
            num++;
            Right += 2;

            while (Right <= sides) {                
                for (int j = 0; j < Down; j++) { // Move down
                    if (j == Down - 1)
                        sum += num;
                    grid[lastX + 1, lastY] = num;
                    lastX += 1;
                    num++;
                }
                for (int j = 0; j < Left; j++) { // Move left
                    if (j == Left - 1)
                        sum += num;
                    grid[lastX, lastY - 1] = num;
                    lastY -= 1;
                    num++;
                }
                for (int j = 0; j < Up; j++) { // Move up
                    if (j == Up - 1)
                        sum += num;
                    grid[lastX - 1, lastY] = num;
                    lastX -= 1;
                    num++;
                }
                if (Right == sides) // stop at end
                    Right -= 1;
                for (int j = 0; j < Right; j++) { // Move right
                    if (j == Right - 2) {
                        if (Right == sides - 1)
                            sum += num + 1;
                        else
                            sum += num;
                    }
                    grid[lastX, lastY + 1] = num;
                    lastY += 1;
                    num++;
                }

                Right += 2;
                Down += 2;
                Left += 2;
                Up += 2;
            }
            sw.Stop();

            Console.WriteLine("Sum of Corners: {0}", sum);
            Console.WriteLine("Elapsed: {0}ms", sw.ElapsedMilliseconds);
            Console.ReadLine();
        }
    }
}
