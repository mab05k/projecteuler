﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem26
{
    class Program
    {
        // A unit fraction contains 1 in the numerator. 
        // The decimal representation of the unit fractions with denominators 2 to 10 are given:

        //1/2	= 	0.5
        //1/3	= 	0.(3)
        //1/4	= 	0.25
        //1/5	= 	0.2
        //1/6	= 	0.1(6)
        //1/7	= 	0.(142857)
        //1/8	= 	0.125
        //1/9	= 	0.(1)
        //1/10	= 	0.1
        // Where 0.1(6) means 0.166666..., and has a 1-digit recurring cycle. 
        // It can be seen that 1/7 has a 6-digit recurring cycle.

        // Find the value of d < 1000 for which 1/d contains the longest recurring cycle in its decimal fraction part.

        static void Main(string[] args)
        {
            int sequenceLength = 0;
            int d = 0;
            for (int i = 983; i > 0; i--)
            {
                if (sequenceLength > i)
                    break;

                int[] foundRemainders = new int[i];
                int value = 1;
                int position = 0;

                while (foundRemainders[value] == 0 && value != 0)
                {
                    foundRemainders[value] = position;
                    value *= 10;
                    value %= i;
                    position++;
                }

                if (position - foundRemainders[value] > sequenceLength)
                {
                    sequenceLength = position - foundRemainders[value];
                    d = i;
                }
            }

            Console.WriteLine("Sequence Length: {0}", sequenceLength);
            Console.WriteLine("d = {0}", d);
            Console.ReadLine();
        }

        public static int ParseRecurrent(decimal n)
        {
            int start = 2;
            
            char[] arr = n.ToString().ToCharArray();

            if (arr.Length == 3) // If divides quickly move on
                //return new char[0];
                return 0;
            for (int i = start; i < arr.Length; i++) // Start past all the 0s
            {
                if (arr[i] == 0)
                    start++;
                else
                    break;
            }

            char lastChar = arr[start]; // Check for repeaters
            for (int i = start; i < arr.Length - 1; i++)
            {
                if (lastChar == arr[i + 1])
                    //return new char[0];
                    return 0;
                lastChar = arr[i + 1];
            }

            int count = 0;
            for (int i = 2; i < arr.Length; i++)
            {
                char[] a1 = new char[i];
                char[] a2 = new char[i];
                count = 0;
                for (int j = 0; j < i-1; j++)
                {
                    a1[j] = arr[start + j];
                    if (start + j + i < arr.Length)
                    {
                        a2[j] = arr[start + j + i];

                        if (arr[start + j] != arr[start + j + i])
                            break;
                        else
                            count++;
                    }
                }
                if (count > 0)
                {
                    //Console.WriteLine(count);
                    break;
                }
            }
            return count;
        }
    }
}
