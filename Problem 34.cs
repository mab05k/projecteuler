﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem34
{
    class Program
    {

//        145 is a curious number, as 1! + 4! + 5! = 1 + 24 + 120 = 145.

//Find the sum of all numbers which are equal to the sum of the factorial of their digits.

//Note: as 1! = 1 and 2! = 2 are not sums they are not included.

        static void Main(string[] args)
        {
            List<int> goodNums = new List<int>();
            int theSum = 0;

            for (int number = 3; number < 1000000; number++)
            {
                //int number = 145;
                string numStr = number.ToString();

                int numberSum = 0;
                for (int i = 0; i < numStr.Length; i++)
                {
                    int factorialSum = 1;
                    int digit = int.Parse(numStr.Substring(i, 1));
                    for (int j = digit; j > 1; j--)
                    {
                        factorialSum *= j;
                    }
                    numberSum += factorialSum;
                }
                if (number == numberSum)
                    goodNums.Add(number);
            }

            foreach (var num in goodNums)
            {
                theSum += num;
            }
            Console.WriteLine(theSum);
            Console.ReadLine();
        }
    }
}
