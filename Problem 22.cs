﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Problem22
{
    public class Program
    {
        // Using names.txt (right click and 'Save Link/Target As...'), a 46K text file containing over five-thousand first names, 
        // begin by sorting it into alphabetical order. 
        // Then working out the alphabetical value for each name, multiply this value by its alphabetical position in the list to obtain a name score.

        // For example, when the list is sorted into alphabetical order, COLIN, which is worth 3 + 15 + 12 + 9 + 14 = 53, is the 938th name in the list.
        // So, COLIN would obtain a score of 938 × 53 = 49714.

        //What is the total of all the name scores in the file?

        public static void Main(string[] args)
        {
            BigInteger bigInt = new BigInteger();
            string[] splitLine = null;
            Stopwatch sw = new Stopwatch();
            sw.Start();

            StreamReader sr = new StreamReader(@"D:\4. Programs\Programming\ProjectEuler\Problem22\names.txt");
            splitLine = sr.ReadLine().Split(',');
            for (int i = 0; i < splitLine.Length; i++) {
                splitLine[i] = splitLine[i].Substring(1, splitLine[i].Length - 2);
            }
            Array.Sort(splitLine);

            for (int i = 0; i < splitLine.Length; i++) {
                int sum = 0;
                for (int j = 0; j < splitLine[i].Length; j++) {
                    sum += Program.GetLetterScore(splitLine[i].Substring(j, 1));
                }
                bigInt += sum * (i + 1);
            }
            sw.Stop();

            Console.WriteLine("Total of Name Scores: {0}", bigInt);
            Console.WriteLine("Elapsed: {0} ms", sw.ElapsedMilliseconds);
            Console.ReadLine();
        }

        public static int GetLetterScore(string letter)
        {
            int score = 0;
            string[] letters = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P",
                "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
            for (int i = 0; i < letters.Length; i++) {
                if (letter == letters[i]) {
                    score = i + 1;
                    break;
                }
            }
            return score;
        }
    }
}
