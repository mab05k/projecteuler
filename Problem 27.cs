﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem27
{
        // Euler discovered the remarkable quadratic formula:

        // n² + n + 41

        // It turns out that the formula will produce 40 primes for the consecutive values n = 0 to 39. 
        // However, when n = 40, 402 + 40 + 41 = 40(40 + 1) + 41 is divisible by 41, and certainly when n = 41, 41² + 41 + 41 is clearly divisible by 41.

        // The incredible formula  n² − 79n + 1601 was discovered, which produces 80 primes for the consecutive values n = 0 to 79. 
        // The product of the coefficients, −79 and 1601, is −126479.

        // Considering quadratics of the form:

        // n² + an + b, where |a| < 1000 and |b| < 1000

        // where |n| is the modulus/absolute value of n
        // e.g. |11| = 11 and |−4| = 4
        // Find the product of the coefficients, a and b, for the quadratic expression that produces the maximum number of 
        // primes for consecutive values of n, starting with n = 0.

    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch sw = new Stopwatch();
            double consecutiveValues = 0;
            double mostValues = 0;
            double a = 0;
            double b = 0;

            sw.Start();
            for (double i = 1000; i >= -1000; i--) {
                if (IsPrime(Math.Abs(i))) {
                    for (double j = 1000; j >= -1000; j--) {
                        if (IsPrime(Math.Abs(j))) {
                            consecutiveValues = 0;

                            double n = 0;
                            while (IsPrime(Math.Abs((n * n) + i * n + j))) {
                                consecutiveValues++;
                                n++;
                            }

                            if (consecutiveValues >= mostValues) {
                                mostValues = consecutiveValues;
                                a = i;
                                b = j;
                            }
                       }
                    }
                }
            }
            sw.Stop();

            Console.WriteLine("A: {0} / B: {1}", a, b);
            Console.WriteLine("Most Consecutive Values: {0}", mostValues);
            Console.WriteLine("Product of Coefficients: {0}", a * b);
            Console.WriteLine("Elapsed: {0}ms", sw.ElapsedMilliseconds);
            Console.ReadLine();

            //Console.WriteLine(IsPrime(79));
            //Console.WriteLine(IsPrime(1601));
            //Console.WriteLine(IsPrime(41));

            Console.ReadLine();
        }

        static bool IsPrime(double x)
        {
            for (int i = 2; i <= Math.Sqrt(x); i++)
                if (x % i == 0)
                    return false;
            return true;
        }
    }
}
