﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem3_Try2
{
    class Program
    {
        // The prime factors of 13195 are 5, 7, 13 and 29.

        // What is the largest prime factor of the number 600851475143 ?

        static void Main(string[] args)
        {
            const long theNumber = 600851475143;
            long number = theNumber;
            int thePrimeFactor = 0;
            Stopwatch sw = new Stopwatch();
            sw.Start();

            while (number > 1) {
                int nextFactor = 2;

                if (number % nextFactor > 0) {
                    nextFactor = 3;
                    do {
                        if (number % nextFactor == 0)
                            break;

                        nextFactor += 2;
                    } while (nextFactor < number);
                }

                number /= nextFactor;
                if (thePrimeFactor < nextFactor)
                    thePrimeFactor = nextFactor;
            }
            sw.Stop();

            Console.WriteLine("Time Elapsed: {0} ticks", sw.ElapsedTicks);
            Console.WriteLine("Largest Prime Factor: " + thePrimeFactor);
            Console.ReadLine();
        }
    }
}
