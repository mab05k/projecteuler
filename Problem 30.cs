﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem30
{
    class Program
    {

//        Surprisingly there are only three numbers that can be written as the sum of fourth powers of their digits:

//1634 = 14 + 64 + 34 + 44
//8208 = 84 + 24 + 04 + 84
//9474 = 94 + 44 + 74 + 44
//As 1 = 14 is not a sum it is not included.

//The sum of these numbers is 1634 + 8208 + 9474 = 19316.

//Find the sum of all the numbers that can be written as the sum of fifth powers of their digits.

        static void Main(string[] args)
        {
            double exponent = 5;
            double total = 0;
            List<double> goodSums = new List<double>();

            Stopwatch sw = new Stopwatch();
            sw.Start();
            for (int i = 2; i < 1000000; i++)
            {
                string number = i.ToString();
                double sum = 0;
                for (int j = 0; j < number.Length; j++)
                {                    
                    double digit = double.Parse(number.Substring(j, 1));
                    sum += Math.Pow(digit, exponent);
                }
                if (sum == i)
                    goodSums.Add(i);

            }
            sw.Stop();
            foreach (var num in goodSums)
            {
                total += num;
            }

            Console.WriteLine("Total: {0}", total);
            Console.WriteLine("Elapsed: {0}ms", sw.ElapsedMilliseconds);
            Console.ReadLine();
        }
    }
}
