﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem12
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // Find the first triangle number with over 500 divisors
            int divisors = 0;
            long theNumber = 0;
            while (divisors < 4)
            {
                for (int i = 1; i < 500*500; i++)
                {
                    theNumber += i;
                    divisors = 0;
                        for (int j = 1; (j*j) <= theNumber; j++)
                            if (theNumber % j == 0)
                                divisors+=2;
                    if (divisors >= 500)
                        break;
                }
            }

            Console.WriteLine(theNumber);
            Console.ReadLine();
        }
    }
}
