﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SummationOfPrimes_Problem10
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // The sum of primes below 10 is: 2 + 3 + 5 + 7 = 17
            // Find the sum of primes below 2,000,000

            Stopwatch sw = new Stopwatch();
            //List<double> primes = new List<double>();
            //double num = 2;
            int limit = 2000000;
            //int notPrime = 0;
            long sum = 2;

            sw.Start();

            for (int i = 3; i < limit; i += 2) {
                bool prime = true;
                for (int j = 3; (j * j) <= i; j++) {
                    if (i % j == 0) {
                        prime = false;
                        break;
                    }
                }
                if (prime)
                    sum += i;
            }
            sw.Stop();

            Console.WriteLine("Sum of Primes: {0}", sum);
            Console.WriteLine("Elapsed: {0} secs", (double)(sw.ElapsedMilliseconds * .001));
            Console.ReadLine();
        }
    }
}
