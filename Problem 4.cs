﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LargestPalindromeProduct_Problem4
{
    class Program
    {
        // A palindromic number reads the same both ways. 
        // The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.

        // Find the largest palindrome made from the product of two 3-digit numbers.

        public static void Main(string[] args)
        {
            int largestPalindrome = 0;

            Stopwatch sw = new Stopwatch();
            sw.Start();
            //for (int i = 100; i < 999; i++)
            //    for (int j = 100; j < 999; j++)
            //    {
            //        int num = (i * j);
            //        if (IsPalindrome(num))
            //            if ((num > largestPalindrome))
            //                largestPalindrome = num;
            //    }

            while (largestPalindrome == 0)
            {
                for (int i = 999; i > 0; i--)
                    for (int j = 999; j > 0; j--) {
                        int num = (i * j);
                        if (IsPalindrome(num))
                            if ((num > largestPalindrome))
                                largestPalindrome = num;
                    }
            }
            sw.Stop();
            Console.WriteLine("Time: {0} ms", sw.ElapsedMilliseconds);
            Console.WriteLine("Largest Palindrome: {0}", largestPalindrome);
            Console.ReadLine();
        }

        public static bool IsPalindrome(int num)
        {            
            string number = num.ToString();
            int i = 0;
            int j = number.Length - 1;
            while (i < j) {
                if (number[i] != number[j])
                    return false;
                i++;
                j--;
            }
            return true;
        }
    }
}
