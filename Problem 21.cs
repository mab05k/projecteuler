﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem21
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // Let d(n) be defined as the sum of proper divisors of n (numbers less than n which divide evenly into n).
            // If d(a) = b and d(b) = a, where a ≠ b, then a and b are an amicable pair and each of a and b are called amicable numbers.

            // For example, the proper divisors of 220 are 1, 2, 4, 5, 10, 11, 20, 22, 44, 55 and 110; therefore d(220) = 284. 
            // The proper divisors of 284 are 1, 2, 4, 71 and 142; so d(284) = 220.

            // Evaluate the sum of all the amicable numbers under 10000.

            double theTotal = 0;

            for (int i = 0; i <= 10000; i++)
            {
                double sumA = 1;
                double sumB = 1;

                for (int factor = 2; factor < Math.Sqrt(i); factor++) {
                    if (i % factor == 0) {
                        sumA += factor;
                        sumA += i / factor;
                    }
                }                
                for (int factor = 2; factor < Math.Sqrt(sumA); factor++) { // Find factors of the Sum of the first factors
                    if (sumA % factor == 0) {
                        sumB += factor;
                        sumB += sumA / factor;
                    }
                }

                if (sumB == i) { // Compare sum of first factors to first number
                    if (sumA != sumB)
                        theTotal += sumA;
                }
            }

            Console.WriteLine("Total: {0}", theTotal);
            Console.ReadLine();
        }
    }
}
