﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem33
{
    class Program
    {

//        The fraction 49/98 is a curious fraction, as an inexperienced mathematician in attempting to simplify it 
        //may incorrectly believe that 49/98 = 4/8, which is correct, is obtained by cancelling the 9s.

//We shall consider fractions like, 30/50 = 3/5, to be trivial examples.

//There are exactly four non-trivial examples of this type of fraction, less than one in value, and containing two 
        //digits in the numerator and denominator.

//If the product of these four fractions is given in its lowest common terms, find the value of the denominator.

        static void Main(string[] args)
        {
            decimal top = 1;
            decimal bottom = 1;
            for (int i = 1; i < 10; i++) {
                for (int j = 1; j < 10; j++) {
                    decimal numerator = decimal.Parse(i.ToString() + j.ToString());
                    decimal numerator2 = i;

                    for (int x = 1; x < 10; x++) {
                        decimal denominator = decimal.Parse(j.ToString() + x.ToString());
                        decimal denominator2 = x;
                        if (numerator < denominator) {
                            if ((numerator / denominator) == (numerator2 / denominator2))
                            {
                                Console.WriteLine(numerator + " / " + denominator);
                                top *= numerator;
                                bottom *= denominator;
                            }                                
                        }
                    }
                }
            }

            Console.WriteLine(top + " / " + bottom);

            for (int x = 0; x < 2; x++) {
                for (int i = 2; i < 20; i++) {
                    if ((top % i == 0) && (bottom % i == 0)) {
                        top /= i;
                        bottom /= i;
                    }
                }
            }

            Console.WriteLine(top + " / " + bottom);
            Console.ReadLine();
        }
    }
}
