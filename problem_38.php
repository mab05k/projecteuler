<?php

//Take the number 192 and multiply it by each of 1, 2, and 3:

//192 × 1 = 192
//192 × 2 = 384
//192 × 3 = 576
//By concatenating each product we get the 1 to 9 pandigital, 192384576. We will call 192384576 the concatenated product of 192 and (1,2,3)

//The same can be achieved by starting with 9 and multiplying by 1, 2, 3, 4, and 5, giving the pandigital, 918273645, which is the concatenated product of 9 and (1,2,3,4,5).

//What is the largest 1 to 9 pandigital 9-digit number that can be formed as the concatenated product of an integer with (1,2, ... , n) where n > 1?


$num = 10000;
$done = false;
$the_number = '';
$pandigitals = array();

while (!$done && $num > 0) {
    $num_string = '';
    $counter = 0;
    $ns = '';
    while (keep_going(str_split($num_string))) {
        $counter++;
        $ns .= $num . ' x ' . $counter . ' = ' . $num * $counter . PHP_EOL;
        $num_string .= $num * $counter;
        if (nine_digits(str_split($num_string))) {
            if (is_pandigital(str_split($num_string)) && $counter > 1) {
                //$done = true;
                $the_number = $num_string;
                $pandigitals[] = $num_string;
            }
            break;
        }     
    }
    $num--;
}

echo 'Largest Pandigital: ' . max($pandigitals) . PHP_EOL;

function is_pandigital($digits) {
    if (count($digits) != 9)
        return false;
    $pandigital_array = array();
    foreach ($digits as $digit) {
        $pandigital_array[$digit] = 1;
    }

    for ($i = 1; $i <= 9; $i++) {
        if (!array_key_exists($i, $pandigital_array))
            return false;
    }

    return true;
}

function nine_digits($num_string) {
    if (count($num_string) != 9)
        return false;
    else
        return true;
}

function keep_going($num_string) {
    if (count($num_string) <= 9)
        return true;
    else
        return false;
}
