﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PythagoreanTriplet_Problem9
{
    public class Program
    {

//        A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,

//a2 + b2 = c2

//For example, 32 + 42 = 9 + 16 = 25 = 52.

//There exists exactly one Pythagorean triplet for which a + b + c = 1000.
//Find the product abc.

        public static void Main(string[] args)
        {
            List<int> a = new List<int>();
            int product = 1;

            for (int i = 1; i < 1000; i++) {
                for (int j = i + 1; j < 1000; j++) {
                    for (int k = j + 1; k < 1000; k++) {
                        if ((i * i) + (j * j) == (k * k) && (i + j + k) == 1000)
                        {
                            a.Add(i);
                            a.Add(j);
                            a.Add(k);
                            product = i * j * k;
                        }
                    }
                }
            }

            Console.WriteLine("Product: {0}", product);
            Console.ReadLine();
        }
    }
}
