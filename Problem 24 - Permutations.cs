﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem24
{
    public class Permutation
    {
        int[] perm = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        int element = -1;

        public void GetPermutation(int k)
        {
            element++;
            perm.SetValue(element, k);

            if (element == perm.Length - 1)
            {
                for (int i = 0; i < perm.Length; i++)
                {
                    Console.Write(perm[i].ToString());
                }
            }
            else
            {
                for (int i = 0; i < perm.Length; i++)
                {
                    if (perm[i] == 0)
                        GetPermutation(i);
                }
            }
            element--;
            perm.SetValue(0, k);
        }
    }
}
