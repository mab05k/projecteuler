﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Numerics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem25
{
    public class Program
    {
        // The Fibonacci sequence is defined by the recurrence relation:

        //Fn = Fn−1 + Fn−2, where F1 = 1 and F2 = 1.
        //Hence the first 12 terms will be:

        //F1 = 1
        //F2 = 1
        //F3 = 2
        //F4 = 3
        //F5 = 5
        //F6 = 8
        //F7 = 13
        //F8 = 21
        //F9 = 34
        //F10 = 55
        //F11 = 89
        //F12 = 144
        //The 12th term, F12, is the first term to contain three digits.

        //What is the first term in the Fibonacci sequence to contain 1000 digits?

        public static void Main(string[] args)
        {
            var prev = new BigInteger(0);
            var next = new BigInteger(1);
            var sum = new BigInteger(0);
            string sumString = "";
            int iteration = 1;
            Stopwatch sw = new Stopwatch();
            sw.Start();
            while (sumString.Length < 1000)
            {
                sum = prev + next;
                sumString = sum.ToString();
                prev = next;
                next = sum;
                iteration++;
            }
            sw.Stop();
            //Console.WriteLine(sumString);
            Console.WriteLine("First Fib # with 1000 digits: {0}", iteration);
            Console.WriteLine("Elapsed: {0}ms", sw.ElapsedMilliseconds);
            Console.ReadLine();
        }
    }
}
