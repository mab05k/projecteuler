﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmallestMultiple_Problem5
{
    class Program
    {
        static void Main(string[] args)
        {
            //2520 is the smallest number that can be divided by each 
            //of the numbers from 1 to 10 without any remainder.
            //What is the smallest positive number that is evenly 
            //divisible by all of the numbers from 1 to 20?
            long theNumber = 0;
            List<int> factors = new List<int>();
            int total = 0;

            Stopwatch sw = new Stopwatch();
            sw.Start();

            while (total < 20)
            {
                theNumber += 2520;
                total = 0;
                factors.Clear();
                for (int i = 1; i <= 20; i++)
                    if (IsDivisible(theNumber, i))
                    {                        
                        total++;
                        factors.Add(i);
                    }
            }

            sw.Stop();

            foreach (var fac in factors)
            {
                Console.Write(fac + " ");
            }
            Console.WriteLine();
            Console.WriteLine(theNumber);
            Console.WriteLine("Elapsed: {0} seconds", sw.ElapsedMilliseconds / 1000);
            Console.ReadLine();
        }

        public static bool IsDivisible(long number, int factor)
        {
            if (number % factor == 0)
                return true;
            return false;
        }
    }
}
