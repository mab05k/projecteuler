﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _10001stPrime_Problem7
{
    public class Program
    {
        static void Main(string[] args)
        {
            // What is the 10001st Prime Number??

            List<long> primeList = new List<long>();
            Stopwatch sw = new Stopwatch();
            long num = 2;
            sw.Start();
            while (primeList.Count < 10001)
            {
                int error = 0;
                if (num == 2)
                {
                    primeList.Add(num);
                    num++;
                }

                for (int i = 2; i < num; i++)
                {
                    if (num % i == 0)
                    {
                        error++;
                        break;
                    }
                }
                if (error == 0)
                    primeList.Add(num);
                num += 2;
            }
            sw.Stop();

            Console.WriteLine("The 10001 prime number is: " + primeList[primeList.Count - 1]);
            Console.WriteLine("Completed in: {0} ms", sw.ElapsedMilliseconds);
            Console.ReadLine();
        }
    }
}
