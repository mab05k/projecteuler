﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem20
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // n! means n × (n − 1) × ... × 3 × 2 × 1

            // For example, 10! = 10 × 9 × ... × 3 × 2 × 1 = 3628800,
            // and the sum of the digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.

            // Find the sum of the digits in the number 100!

            int theNumber = 100;
            int sum = 0;
            BigInteger factorial = new BigInteger(theNumber);            

            for (int i = theNumber - 1; i > 0; i--) {
                factorial *= i;
            }

            string digits = factorial.ToString();
            List<int> digList = new List<int>();

            for (int i = 0; i < digits.Length; i++)            {
                digList.Add(int.Parse(digits.Substring(i, 1)));
            }
            foreach (var digit in digList) {
                sum += digit;
            }

            Console.WriteLine("{0}!: {1}", theNumber, factorial);
            Console.WriteLine("Sum of Digits: {0}", sum);
            Console.ReadLine();
        }
    }
}
