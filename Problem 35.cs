﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem35
{
    class Program
    {
//        The number, 197, is called a circular prime because all rotations of the digits: 197, 971, and 719, are themselves prime.

//There are thirteen such primes below 100: 2, 3, 5, 7, 11, 13, 17, 31, 37, 71, 73, 79, and 97.

//How many circular primes are there below one million?

        static void Main(string[] args)
        {
            //new Program().BruteForce();
            int[] perm = new int[] { 1,1,0 };
            IEnumerable<int> permu = perm.OrderBy(i => i);
            int y = 0;
            foreach (var num in permu) {
                perm[y] = num;
                y++;
            }

            int count = 1;
            int numPerm = perm.Length;
            
            for (int i = 1; i < perm.Length; i++) {
                numPerm *= i;
            }

            for (int x = 0; x < perm.Length; x++) {
                Console.Write(perm[x]);
            }
            Console.WriteLine(); 

            while (count < numPerm)
            {
                int N = perm.Length;
                int i = N - 1;

                while (perm[i - 1] >= perm[i])
                {
                    i = i - 1;
                }

                int j = N;
                while (perm[j - 1] <= perm[i - 1])
                {
                    j = j - 1;
                }

                // swap values at position i-1 and j-1
                swap(i - 1, j - 1, perm);

                i++;
                j = N;

                while (i < j)
                {
                    swap(i - 1, j - 1, perm);
                    i++;
                    j--;
                }
                count++;

                for (int x = 0; x < perm.Length; x++)
                {
                    Console.Write(perm[x]);
                }
                Console.WriteLine();  
            }

            string permNum = "";
            for (int k = 0; k < perm.Length; k++)
            {
                permNum = permNum + perm[k];
            }

            Console.ReadLine();
        }

        //int[] perm = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        //public void BruteForce()
        //{
        //    Stopwatch clock = Stopwatch.StartNew();

        //    int count = 1;
        //    int numPerm = perm.Length;
        //    for (int i = 1; i < perm.Length; i++)
        //    {
        //        numPerm *= i;
        //    }

        //    while (count < numPerm)
        //    {
        //        int N = perm.Length;
        //        int i = N - 1;

        //        while (perm[i - 1] >= perm[i])
        //        {
        //            i = i - 1;
        //        }

        //        int j = N;
        //        while (perm[j - 1] <= perm[i - 1])
        //        {
        //            j = j - 1;
        //        }

        //        // swap values at position i-1 and j-1
        //        swap(i - 1, j - 1);

        //        i++;
        //        j = N;

        //        while (i < j)
        //        {
        //            swap(i - 1, j - 1);
        //            i++;
        //            j--;
        //        }
        //        count++;
        //    }

        //    string permNum = "";
        //    for (int k = 0; k < perm.Length; k++)
        //    {
        //        permNum = permNum + perm[k];
        //    }


        //    clock.Stop();
        //    Console.WriteLine("The millionth lexicographic permutation is: {0}", permNum);
        //    Console.WriteLine("Solution took {0} ms", clock.ElapsedMilliseconds);
        //}

        private static void swap(int i, int j, int[] perm)
        {
            int k = perm[i];
            perm[i] = perm[j];
            perm[j] = k;
        }
    }
}
