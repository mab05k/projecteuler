﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FibonnaciNumbers_Problem2
{
    public class Program
    {
        static void Main(string[] args)
        { // Find the sum of the even valued Fibonnaci Numbers
            int x = 0;
            int y = 1;
            int z = 0;
            int total = 0;

            while (z < 4000000) {
                z = x + y;
                x = y;
                y = z;
                if (z % 2 == 0)
                    total += z;
            }

            Console.WriteLine("Sum of even fibonnaci numbers < 4000000: {0}", total);
            Console.ReadKey();
        }
    }
}
