﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SumSquarDifference_Problem6
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // Sum of squares (1^2 + 2^2 + ... + 10^2) = 385
            // Square of sum (1 + 2 + ... + 10)^2 = 55^2 = 3025
            // Difference 3025 - 385 = 2640
            // Find the difference for the first 100 

            long sumOfSquares = 0;
            long squareOfSum = 0;

            for (int i = 1; i < 101; i++)
            {
                sumOfSquares += i * i;
                squareOfSum += i;
            }

            Console.WriteLine("Sum of Squares: {0}", sumOfSquares);
            Console.WriteLine("Square of Sum: {0}", squareOfSum * squareOfSum);
            Console.WriteLine();
            Console.WriteLine("Difference = {0}", (squareOfSum * squareOfSum) - sumOfSquares);

            Console.ReadLine();
        }
    }
}
