﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem19
{
    public class Program
    {
        public static void Main(string[] args)
        {
//            You are given the following information, but you may prefer to do some research for yourself.
//              1 Jan 1900 was a Monday.
//              Thirty days has September,
//              April, June and November.
//              All the rest have thirty-one,
//              Saving February alone,
//              Which has twenty-eight, rain or shine.
//              And on leap years, twenty-nine.
//          A leap year occurs on any year evenly divisible by 4, but not on a century unless it is divisible by 400.
//          How many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?

            int sundays = 0;

            // This is kind of cheap... it relies on the C# language so solve the logic
            for (DateTime d = new DateTime(1901, 1, 1); d <= new DateTime(2000, 12, 31); d = d.AddMonths(1))
            {
                if (d.DayOfWeek == DayOfWeek.Sunday)
                    sundays++;
            }

            #region myCode
            // years 1901 - 2000
                //for (int year = 1901; year <= 2000; year++)
                //{
                //    for (int month = 1; month <= 12; month++)
                //    {
                //        int days = 0;
                //        switch (month)
                //        {
                //            case 1:
                //            case 3:
                //            case 5:
                //            case 7:
                //            case 8:
                //            case 10:
                //            case 12:
                //                days = 31;
                //                break;
                //            case 4:
                //            case 6:
                //            case 9:
                //            case 11:
                //                days = 30;
                //                break;
                //            case 2:
                //                if (Program.LeapYear(year))
                //                    days = 29;
                //                else
                //                    days = 28;
                //                break;
                //            default:
                //                break;
                //        }
                //        for (int day = 1; day <= days; day++)
                //        {
                //            DateTime theDay = new DateTime(year, month, day);

                //            if (day == 1 && theDay.DayOfWeek == DayOfWeek.Sunday)
                //            {
                //                Console.WriteLine("{0}/{1}/{2}", day, month, year);
                //                sundays++;
                //            }
                //        }
                //    }
            //}
            #endregion

            Console.WriteLine(sundays);
            Console.ReadLine();
        }

        #region isLeapYear
        public static bool LeapYear(int year)
        {
            bool isLeapYear = false;
            if (year % 4 == 0)
            {
                isLeapYear = true;
            }
            return isLeapYear;
        }
        #endregion
    }
}
