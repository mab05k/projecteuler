﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project16
{
    public class Program
    {

        //215 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.

        //What is the sum of the digits of the number 21000?

        public static void Main(string[] args)
        {
            BigInteger bigInt = new BigInteger(1);
            int sum = 0;

            for (int i = 0; i < 1000; i++)
            {
                bigInt *= 2;
            }
            string intString = bigInt.ToString();
            for (int i = 0; i < intString.Length; i++)
            {
                sum += int.Parse(intString[i].ToString());
            }

            Console.WriteLine("Number: {0}", bigInt.ToString());
            Console.WriteLine("Sum: {0}", sum.ToString());
            Console.ReadLine();
        }
    }
}
