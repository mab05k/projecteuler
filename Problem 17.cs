﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem17
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // If the numbers 1-5 are written out (one, two, three, four, five)
            // then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total
            // If 1-1000 were written out, how many letters will be used

            int count = 0;
            string[] ones = { "", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine" };
            string[] tens = { "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };
            string[] hundreds = {"","onehundred","twohundred","threehundred","fourhundred",
                                    "fivehundred","sixhundred", "sevenhundred","eighthundred","ninehundred"};            
            string[] teens = {"ten","eleven","twelve","thirteen","fourteen","fifteen",
                                 "sixteen","seventeen","eighteen","nineteen"};
            string oneThousand = "onethousand";
            string and = "and";
            
            for (int h = 0; h < hundreds.Length; h++) {
                for (int t = 0; t < teens.Length; t++) {
                    if (hundreds[h] == "") {
                        count += hundreds[h].Length + ones[t].Length;
                        count += hundreds[h].Length + teens[t].Length;
                    }
                    else {
                        if (ones[t] == "") {
                            count += hundreds[h].Length;
                            count += hundreds[h].Length + and.Length + teens[t].Length;
                        }
                        else {
                            count += hundreds[h].Length + and.Length + ones[t].Length;
                            count += hundreds[h].Length + and.Length + teens[t].Length;
                        }
                    }
                }                

                for (int i = 0; i < tens.Length; i++) {
                    for (int j = 0; j < ones.Length; j++) {
                        if (hundreds[h] == "") {
                            count += tens[i].Length + ones[j].Length;
                        }
                        else {
                            count += hundreds[h].Length + and.Length + tens[i].Length + ones[j].Length;
                        }
                    }
                }
            }

            count += oneThousand.Length;
            Console.WriteLine("Letters: {0}", count.ToString());
            Console.ReadLine();
        }
    }
}
